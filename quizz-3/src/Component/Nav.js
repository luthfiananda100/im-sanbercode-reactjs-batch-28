import React from "react"
import Logo from '../Public/logo.png'
import { Link } from 'react-router-dom'
import { Button, Tooltip } from 'antd'
import { SearchOutlined } from '@ant-design/icons';

const Nav = () => {
    return (
        <>
            <div className="topnav">
                <Link to="/" ><img src={Logo} width="70" alt="This Is Logo" /></Link>
                <Link to="/" >Home</Link>
                <Link to="/mobile-list" >Movie List</Link>
                <Link to="/about" >About</Link>
                <form>
                    <input type="text" style={{ marginRight:'10px', borderRadius:'10px' }} />
                    <Tooltip title="search">
                        <Button type="primary" shape="circle" icon={<SearchOutlined />} />
                    </Tooltip>
                </form>
            </div>

        </>
    )
}

export default Nav;