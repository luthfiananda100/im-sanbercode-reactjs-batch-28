import React, { useContext, useEffect } from "react";
import { contextMobile } from "../Context/ContextMobile";

const MobileHome = () => {
    const { fetchStatus, setfetchStatus, daftarMobile, functions } = useContext(contextMobile);
    const { fetchData, convertSize, platform } = functions;

    // const [me, setme] = useState(true);

    // const handle = () =>{
    //     console.log(daftarMobile);
    // }

    useEffect(() => {
        if (fetchStatus === false) {
            fetchData();
            setfetchStatus(!fetchStatus)
        }
    })

    return (
        <>
            <div className="row">
                <div className="section">
                    <div className="card">
                        {daftarMobile.map((item, index) => {
                            return (
                                <div key={index}>
                                    <h2>{item.name}</h2>
                                    <h5>Release Year : {item.release_year}</h5>
                                    <img className="fakeimg" style={{ width:'50%', height:'300px', objectFit:'cover' }} src={item.image_url} alt="This is Cover Mobile" />
                                    <br />
                                    <br />
                                    <div>
                                        <strong>Price: {item.price===0 ? 'Free' : `Rp${item.price},00`}</strong><br />
                                        <strong>Rating: {item.rating}</strong><br />
                                        <strong>Size: {convertSize(item.size)}</strong><br />
                                        <strong style={{ marginRight:'10px' }}>Platform : {platform(item.is_android_app, item.is_ios_app)}</strong>
                                        <br />
                                    </div>
                                    <p>
                                        <strong style={{ marginRight:'10px' }}>Description :</strong>
                                        <strong>{item.description}</strong>
                                    </p>
        
                                    <hr />
                                </div>
                            )
                        })}
                    </div>

                </div>
            </div>

        </>
    )
}
export default MobileHome