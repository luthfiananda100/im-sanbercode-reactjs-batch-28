import React, { useContext, useEffect } from "react";
import { Button } from 'antd';
import { contextMobile } from "../Context/ContextMobile";
import { Link, useHistory } from "react-router-dom";

const MobileList = () => {

    let history = useHistory();

    const { fetchStatus, setfetchStatus, daftarMobile, functions } = useContext(contextMobile);
    const { fetchData, convertSize, platform, deleteApps } = functions;

    useEffect(() => {
        if (fetchStatus === false) {
            fetchData();
            setfetchStatus(!fetchStatus)
        }
    }, [])

    const handleEdit = event =>{
        let editIndex = parseInt(event.target.value);
        history.push(`mobile-form/edit/${editIndex}`)
    }

    const handleDelete = event =>{
        let delIndex = parseInt(event.target.value);
        deleteApps(delIndex)
    }

    return (
        <>
            <div className="row">
                <div className="section">
                    <div className="card">
                        <h1 style={{ textAlign: "center" }}>Mobile App List</h1>
                        <Link to="/mobile-form"><Button type="primary" style={{ marginBottom: '10px' }}>Create New Mobile App</Button></Link>
                        <table style={{ width:'100%', textAlign:'center' }}>
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Nama</td>
                                    <td>Category</td>
                                    <td>Description</td>
                                    <td>Release Year</td>
                                    <td>Size</td>
                                    <td>Price</td>
                                    <td>Rating</td>
                                    <td>Platform</td>
                                    <td>Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    daftarMobile.map((item, index) => {
                                        return(

                                            <tr key={index}>
                                            <td>{index+1}</td>
                                            <td>{item.name}</td>
                                            <td>{item.category}</td>
                                            <td>{item.description}</td>
                                            <td>{item.release_year}</td>
                                            <td>{convertSize(item.size)}</td>
                                            <td>{item.price === 0 ? 'Free' : `Rp${item.price},00`}</td>
                                            <td>{item.rating}</td>
                                            <td>{platform(item.is_android_app, item.is_ios_app)}</td>
                                            <td>
                                                <Button type="primary" onClick={handleEdit}>Edit</Button>
                                                <Button type="primary" style={{ marginLeft:'10px' }} onClick={handleDelete}>Delete</Button>
                                            </td>

                                        </tr>
                                        )

                                    })
                                }
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </>
    )
}

export default MobileList