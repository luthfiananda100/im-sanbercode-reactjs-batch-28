import React, { useContext, useEffect } from "react";
import { contextMobile } from "../Context/ContextMobile";

const FormMobile = () => {

    const { functions, input, setInput, currentIndex, setcurrentIndex } = useContext(contextMobile);
    const { handleChange, submitApps, updateApps } = functions;

    const handleSubmit = (event) =>{
        event.preventDefault();
        if(currentIndex===null) {
            submitApps(input)
        }else{
            updateApps(currentIndex)
            setcurrentIndex(null)
        }
    }

    useEffect(() => {
        
        setInput({
            name: "",
            description: "",
            category: "",
            release_year: 2007,
            size: 0,
            price: 0,
            rating: 0,
            image_url: "",
            is_android_app: true,
            is_ios_app: true
        })
    }, [])
    return (
        <>
            <div className="row">
                <div className="section">
                    <div className="card">
                        <h1>Mobile Apps Form</h1>
                        <form onSubmit={handleSubmit}>
                            <table>
                                <tbody>
                                    <tr>
                                        <td><label htmlFor="name">Name</label></td>
                                        <td><input type="text" name="name" id="name" onChange={handleChange} value={input.name} required /></td>
                                    </tr>
                                    <tr>
                                        <td><label htmlFor="category">Category</label></td>
                                        <td><input type="text" name="category" id="category" onChange={handleChange} value={input.category} required/></td>
                                    </tr>
                                    <tr>
                                        <td><label htmlFor="description">Description</label></td>
                                        <td><textarea name="description" id="description" cols="30" rows="4" onChange={handleChange} value={input.description} required></textarea></td>
                                    </tr>
                                    <tr>
                                        <td><label htmlFor="release_year">Release Year</label></td>
                                        <td><input type="number" name="release_year" id="release_year" onChange={handleChange} value={input.release_year} required/></td>
                                    </tr>
                                    <tr>
                                        <td><label htmlFor="size">Size (MB)</label></td>
                                        <td><input type="number" name="size" id="size" onChange={handleChange} value={input.size} required/></td>
                                    </tr>
                                    <tr>
                                        <td><label htmlFor="price">Price</label></td>
                                        <td><input type="number" name="price" id="price" onChange={handleChange} value={input.price} required/></td>
                                    </tr>
                                    <tr>
                                        <td><label htmlFor="rating">Rating</label></td>
                                        <td><input type="number" name="rating" id="rating" onChange={handleChange} min="0" max="5" value={input.rating} required /></td>
                                    </tr>
                                    <tr>
                                        <td><label htmlFor="image_url">Image URL</label></td>
                                        <td><input type="url" name="image_url" id="image_url" onChange={handleChange} value={input.image_url} required/></td>
                                    </tr>
                                    <tr>
                                        <td><label htmlFor="is_android_app">Android Platform</label></td>
                                        <td><input type="checkbox" name="is_android_app" checked={input.is_android_app} onChange={handleChange} required /></td>
                                    </tr>
                                    <tr>
                                        <td><label htmlFor="is_ios_app">IOS Platform</label></td>
                                        <td><input type="checkbox" name="is_ios_app" checked={input.is_ios_app} onChange={handleChange} /></td>
                                    </tr>
                                    <tr>
                                        <td><button>Submit</button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default FormMobile