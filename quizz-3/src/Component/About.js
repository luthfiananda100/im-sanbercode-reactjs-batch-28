import React from "react";

const About = () => {
    return (
        <>
            <div className="row">
                <div className="section">
                    <div className="card about-card">
                            <h1 style={{ textAlign: 'center', marginTop:'0px' }}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
                            <ol>
                                <li><b>Nama</b> : Muhammad Luthfi Ananda</li>
                                <li><b>Email</b> : luthfiananda100@gmail.com</li>
                                <li><b>Sistem Operasi yang digunakan</b> : Windows</li>
                                <li><b>Akun Telegram</b> : @luthfi100</li>
                            </ol>
                    </div>
                </div>
            </div>
        </>
    )
}

export default About;