import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

// Component
import Nav from "./Component/Nav";
import Footer from "./Component/Footer";
import MobileHome from "./Component/MobileHome";
import About from "./Component/About";
import MobileList from "./Component/MobileList";
import FormMobile from "./Component/FormMobile";

//Context
import { MobileProvider } from './Context/ContextMobile'

const Routes = () => {
    return (
        <>
            <Router>
                <MobileProvider>
                    <Nav />
                    <Switch>
                        <Route path="/" exact component={MobileHome} />
                        <Route path="/about" exact component={About} />
                        <Route path="/mobile-list" exact component={MobileList} />
                        <Route path="/mobile-form" exact component={FormMobile} />
                        <Route path="/mobile-form/edit/:slug" exact component={FormMobile} />
                        <Route path="/search/:valueOfSearch" exact component={MobileHome} />
                    </Switch>
                    <Footer />
                </MobileProvider>
            </Router>

        </>
    )
}

export default Routes;