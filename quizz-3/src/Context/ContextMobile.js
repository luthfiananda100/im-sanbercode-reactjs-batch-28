import React, { createContext, useState } from "react";
import axios from "axios";
import { useHistory } from "react-router";

export const contextMobile = createContext();

export const MobileProvider = props => {
    let history = useHistory();

    const [fetchStatus, setfetchStatus] = useState(false);
    const [daftarMobile, setdaftarMobile] = useState([]);
    const [input, setInput] = useState({
        name: "",
        description: "",
        category: "",
        release_year: 2007,
        size: 0,
        price: 0,
        rating: 0,
        image_url: "",
        is_android_app: true,
        is_ios_app: true
    })
    const [currentIndex, setcurrentIndex] = useState(null)

    // Get All Data
    const fetchData = async () => {
        const result = await axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps`);
        setdaftarMobile(result.data.map(x => {
            return {
                id: x.id, name: x.name, description: x.description, category: x.category, size: x.size,
                price: x.price, rating: x.rating, image_url: x.image_url, release_year: x.release_year, is_android_app: x.is_android_app, is_ios_app: x.is_ios_app
            }
        }));
    }

    const deleteApps = (id) => {
        axios.delete(`http://backendexample.sanbercloud.com/api/mobile-apps/${id}`)
            .then(() => {
                let newData = daftarMobile.filter(e => { return e.id !== id })
                setdaftarMobile(newData);
            })
    }

    const updateApps = (id) => {
        axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${id}`,
            input).then((res) =>{
            setfetchStatus(false)
            console.log(res)
        })
    }

    //Form
    const handleChange = (event) => {
        let typeOfValue = event.target.value;
        let name = event.target.name;

        setInput({ ...input, [name]: typeOfValue })
    }

    const submitApps = (input) => {
        axios.post('http://backendexample.sanbercloud.com/api/mobile-apps', input)
            .then(res => {
                let data = res.data;
                setdaftarMobile([...daftarMobile,
                {
                    id: data.id,
                    name: data.name,
                    description: data.description,
                    category: data.category,
                    release_year: data.release_year,
                    size: data.size,
                    price: data.price,
                    rating: data.rating,
                    image_url: data.image_url,
                    is_android_app: data.is_android_app,
                    is_ios_app: data.is_ios_app
                }]);
                console.log(data);
            })
        history.push('/mobile-list')

    }



    //Function
    const convertSize = size => {
        var newSize = Number(size / 1000);
        var roundedString = newSize.toFixed(2);
        return `${roundedString} MB`;
    }

    const platform = (andro, ios) => {
        if (andro === 1 && ios === 1) {
            return "Android & IOS"
        } else if (andro === 1 && ios === 0) {
            return "Android"
        } else {
            return "IOS"
        }
    }

    const functions = {
        fetchData,
        convertSize,
        platform,
        deleteApps,
        handleChange,
        submitApps,
        updateApps
    }

    return (
        <contextMobile.Provider value={{
            fetchStatus,
            setfetchStatus,
            daftarMobile,
            setdaftarMobile,
            functions,
            input,
            setInput,
            currentIndex,
            setcurrentIndex
        }}>
            {props.children}
        </contextMobile.Provider>
    )
}