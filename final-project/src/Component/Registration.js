import React, { useContext } from "react";
import './Detail.css'
import registration from '../Public/Image/registration.png'
import { Form, Input, Button } from 'antd';
import { useHistory } from "react-router";
import { contextUser } from "../Context/ContextUser";

const Registration = () => {
    let history = useHistory();

    const {regisInput, setregisInput, functions} = useContext(contextUser)
    const {regisSubmit} = functions


    const handleChange = (event) => {
        let typeOfValue = event.target.value
        let name = event.target.name

        setregisInput({ ...regisInput, [name]: typeOfValue })
    }

    const handleRegis = (e) => {
        regisSubmit();
        history.push('/login-form');
    }

    return (
        <>
            <div className="home">
                <div className="card-detail-container">
                    <div className="card-detail-img">
                        <img src={registration} alt="Regsitration" />
                    </div>
                    <div className="card-detail-detail">
                        <h1 style={{ marginLeft:'160px' }}>Registration Form</h1>
                        <Form
                            style={{ marginTop: '30px' }}
                            name="basic"
                            labelCol={{ span: 5 }}
                            wrapperCol={{ span: 10 }}
                            initialValues={{ remember: true }}
                            onFinish={handleRegis}
                            autoComplete="off"
                        >
                            <Form.Item
                                label="Username"
                                name="name"
                                rules={[{ required: true, message: 'Please input your name!' }]}
                            >
                                <Input name="name" type="text" onChange={handleChange} value={regisInput.name} />
                            </Form.Item>

                            <Form.Item
                                label="Email"
                                name="email"
                                rules={[{ required: true, message: 'Please input your email!' }]}
                            >
                                <Input name="email" type="email" onChange={handleChange} value={regisInput.email} />
                            </Form.Item>

                            <Form.Item
                                label="Password"
                                name="password"
                                rules={[{ required: true, message: 'Please input your password!' }]}
                            >
                                <Input name="password" type="password" onChange={handleChange} value={regisInput.password} />
                            </Form.Item>

                            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                                <Button type="primary" htmlType="submit">
                                    Register
                                </Button>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Registration