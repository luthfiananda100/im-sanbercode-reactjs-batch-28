import './Footer.css'
import React from 'react'

const Footer = () => {
    return (
        <>
            <div className="footer-container">
                <p>MovieGames.id ©2021 Created by Muhammad Luthfi Ananda</p>
            </div>
        </>
    )
}

export default Footer;