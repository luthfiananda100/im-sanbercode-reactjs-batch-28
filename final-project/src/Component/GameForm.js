import React, { useContext, useEffect, useState } from "react";
import { Form, Input, Button, Checkbox } from "antd";
import { useHistory, useParams } from "react-router";
import { contextPreLogin } from "../Context/ContextItem";

const GameForm = () => {
    let history = useHistory()

    let {slug} = useParams()

    const { currentIndex, setcurrentIndex, gameInput, setgameInput, functions } = useContext(contextPreLogin)
    const { addGame, editGame, updateGame } = functions

    useEffect(() => {
        setgameInput({
            name: "",
            genre: "",
            singlePlayer: false,
            multiplayer: false,
            platform: "",
            release: 0,
            image_url: ""
        })
        if(slug!==undefined){
            setcurrentIndex(parseInt(slug))
            editGame(parseInt(slug))
            console.log(gameInput.name);
        }
    }, [])

    const handleChange = (e) => {
        let name = e.target.name

        if (name === "singlePlayer" || name === "multiplayer") {
            var typeOfValue = e.target.checked
            if (typeOfValue) {
                var value = 1
            } else {
                var value = 0
            }
        } else {
            var value = e.target.value
        }
        setgameInput({ ...gameInput, [name]: value })
    }

    const handleaddGame = (e) => {
        console.log(e);
        console.log(gameInput);

        if (currentIndex === null) {
            //Add Game
            addGame()
        } else {
            //Update Game
            updateGame(parseInt(slug))
            setcurrentIndex(null)
            history.push('/dashbor-game-list')
        }

        history.push('/dashbor-game-list')
    }

    return (
        <>
            <div className="dashbor-content">
                <div className="form" style={{ marginRight: 'auto', marginLeft: 'auto', width: '60%', display: 'flex', flexDirection: 'column' }}>
                    <h1 style={{ marginLeft: '40%', marginRight: 'auto' }}>Game Form</h1>
                    <Form
                        labelCol={{ span: 5 }}
                        wrapperCol={{ span: 15 }}
                        layout="horizontal"
                        onFinish={handleaddGame}
                        autoComplete="off"
                    >
                        <Form.Item
                            label="Name"
                            rules={[{ required: true, message: 'Please input Game Name' }]}
                        >
                            <Input type="text" name="name" onChange={handleChange} value={gameInput.name} />
                        </Form.Item>
                        
                        <Form.Item
                            label="Genre"

                            rules={[{ required: true, message: 'Please input Movie Genre' }]}
                        >
                            <Input type="text" name="genre" onChange={handleChange} value={gameInput.genre} />
                        </Form.Item>
                        <Form.Item
                            label="Platform"

                            rules={[{ required: true, message: 'Please input Movie Duration' }]}
                        >
                            <Input type="text" name="platform" onChange={handleChange} value={gameInput.platform} />
                        </Form.Item>
                        <Form.Item
                            label="Image Url"

                            rules={[{ required: true, message: 'Please input Movie Image url' }]}
                        >
                            <Input type="text" name="image_url" onChange={handleChange} value={gameInput.image_url} />
                        </Form.Item>
                        <Form.Item
                            label="Release Year"

                            rules={[{ required: true, message: 'Please input Movie Release' }]}
                        >
                            <Input type="number" name="release" onChange={handleChange} value={gameInput.release} />
                        </Form.Item>

                        <Form.Item
                            label="Single Player"
                        >
                            <Checkbox name="singlePlayer" onChange={handleChange} checked={gameInput.singlePlayer} />
                        </Form.Item>

                        <Form.Item
                            label="Multi Player"
                        >
                            <Checkbox name="multiplayer" onChange={handleChange} checked={gameInput.multiplayer} />
                        </Form.Item>

                        <Form.Item wrapperCol={{ offset: 6, span: 16 }}>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </>
    )
}
export default GameForm