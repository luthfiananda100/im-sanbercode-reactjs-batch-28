import React from "react";
import { Form, Input, Button, message } from "antd";
import { useContext, useEffect } from "react/cjs/react.development";
import { contextUser } from "../Context/ContextUser";
import Cookies from "js-cookie";

const ChangePass = () => {

    const {functions, changepassInput, setchangepassInput} = useContext(contextUser)
    const {changePassword} = functions

    useEffect(()=>{
        setchangepassInput({
            current_password: "",
            new_password: "",
            new_confirm_password: ""
        })
    }, [])

    const handleChange = (event) => {
        let type = event.target.value
        let name = event.target.name
        setchangepassInput({...changepassInput, [name] : type})
    }

    const handlechangePass = (event) => {
        console.log(changepassInput);
        if(changepassInput.new_password===changepassInput.new_confirm_password){
            changePassword(Cookies.get('token'))
        } else{
            message.warning('New Confirm Password not Same')
        }
        
    }

    return (
        <>
            <div className="dashbor-content">
                <h1 style={{ marginLeft:'400px' }}>Account</h1>
                <Form
                    style={{ marginTop:'30px' }}
                    labelCol={{ span: 5 }}
                    wrapperCol={{ span: 10 }}
                    layout="horizontal"
                    onFinish={handlechangePass}
                    autoComplete="off"
                >
                    <Form.Item
                        label="Current Password"
                        rules={[{ required: true, message: 'Please input Game Name' }]}
                    >
                        <Input type="text" name="current_password" onChange={handleChange} value={changepassInput.current_password} />
                    </Form.Item>

                    <Form.Item
                        label="New Password"

                        rules={[{ required: true, message: 'Please input Movie Genre' }]}
                    >
                        <Input type="text" name="new_password" onChange={handleChange} value={changepassInput.new_password}/>
                    </Form.Item>

                    <Form.Item
                        label="Confirm New Password"

                        rules={[{ required: true, message: 'Please input Movie Genre' }]}
                    >
                        <Input type="text" name="new_confirm_password" onChange={handleChange} value={changepassInput.new_confirm_password}/>
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 9, span: 16 }}>
                        <Button type="primary" htmlType="submit">
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </>
    )
}

export default ChangePass