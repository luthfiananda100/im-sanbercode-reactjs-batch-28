import React, { useContext, useEffect } from "react";
import { useParams } from "react-router";
import { contextPreLogin } from "../Context/ContextItem";
import './Detail.css'

const DetailGames = () => {

    const { datagameById, setdatagameById, functions } = useContext(contextPreLogin);
    const { fetchGamesById } = functions;

    let { slug } = useParams();

    useEffect(() => {
        if (slug !== undefined) {
            setdatagameById({
                id: 0,
                name: "",
                genre: "",
                singlePlayer: 0,
                multiplayer: 0,
                platform: "",
                release: 0,
                image_url: ""
            })
            fetchGamesById(parseInt(slug))
        }
    }, [])


    return (
        <>
            <div className="home">
                <div className="card-detail-container">
                    <div className="card-detail-img">
                        <img src={datagameById.image_url} alt="coverMovie" />
                    </div>
                    <div className="card-detail-detail">
                        <strong style={{ fontSize: '30px' }}>Name : {datagameById.name}</strong>
                        <strong>Genre : {datagameById.genre}</strong>
                        <strong>Release : {datagameById.release}</strong>
                        <strong>SinglePlayer :{datagameById.singlePlayer === 1 ? 'YES' : 'NO'}</strong>
                        <strong>Multiplayer : {datagameById.multiplayer === 1 ? 'YES' : 'NO'}</strong>
                        <strong>Platform : {datagameById.platform}</strong>
                        <p>{console.log(datagameById.platform)}</p>
                    </div>
                </div>
            </div>
        </>
    )
}

export default DetailGames