import React, { useEffect, useContext } from "react";
import './Home.css'
import { contextPreLogin } from "../Context/ContextItem";
import { Link } from "react-router-dom";

const HomeGames = () => {

    const {daftarGames, fetchStatusGames, setfetchStatusGames, functions} = useContext(contextPreLogin);
    const {fetchGames} = functions;

    useEffect(() => {
        if (fetchStatusGames) {
            fetchGames();
            setfetchStatusGames(false)
        }
    }, [fetchStatusGames, setfetchStatusGames, fetchGames])

    return (
        <>
            <div className="home">
                <h1>Latest Games</h1>
                <div className="row">
                    {daftarGames !== null && (
                        daftarGames.map((item, index) => {
                            return (
                                <div className="card" key={index}>
                                    <img src={item.image_url} alt="Hello" />
                                    <div className="card-body">
                                        <Link to={`/detail-game/${item.id}`}><h2><strong>{item.name}</strong></h2></Link>
                                        <p>{item.genre}</p>
                                        <p>{item.platform}</p>
                                        <p>{item.release}</p>
                                    </div>
                                </div>
                            )
                        })
                    )}
                </div>
            </div>
        </>
    )
}

export default HomeGames