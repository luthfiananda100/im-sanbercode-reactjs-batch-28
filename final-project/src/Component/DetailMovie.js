import React, { useContext, useEffect } from "react";
import { useParams } from "react-router";
import { contextPreLogin } from "../Context/ContextItem";
import './Detail.css'

const DetailMovie = () => {

    const {datamovieById, setdatamovieById, functions} = useContext(contextPreLogin);
    const {fetchMoviesById} = functions;

    

    let { slug } = useParams();

    useEffect(() => {
        if (slug !== undefined) {
            setdatamovieById({
                description: "",
                genre: "",
                id: 0,
                title: "",
                duration: 0,
                image_url: "",
                rating: 0,
                review: "",
                year: 0
            })
            fetchMoviesById(parseInt(slug))
        }
    }, [])

    return (
        <>
            <div className="home">
                <div className="card-detail-container">
                    <div className="card-detail-img">
                        <img src={datamovieById.image_url} alt="coverMovie" />
                    </div>
                    <div className="card-detail-detail">
                        <strong style={{ fontSize:'30px' }}>Title : {datamovieById.title}</strong>
                        <strong>Genre : {datamovieById.genre}</strong>
                        <strong>Year : {datamovieById.year}</strong>
                        <strong>Duration : {datamovieById.duration} Minutes</strong>
                        <strong>Rating : {datamovieById.rating}</strong>
                        <strong>Review : {datamovieById.review}</strong>
                        <strong>Description : {datamovieById.description}</strong>
                    </div>
                </div>
            </div>
        </>
    )
}

export default DetailMovie