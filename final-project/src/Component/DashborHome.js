import React, { useEffect, useContext } from "react";
import './Home.css'
import { contextPreLogin } from "../Context/ContextItem";
import { Link } from "react-router-dom";

const DashborHome = () => {
    const { fetchStatus, setfetchStatus, daftarMovies, daftarGames, functions } = useContext(contextPreLogin);
    const { fetchGames, fetchMovies } = functions;

    useEffect(() => {
        if (fetchStatus) {
            fetchMovies();
            fetchGames();
            setfetchStatus(false)
        }
    }, [fetchStatus, setfetchStatus, fetchMovies, fetchGames])

    return (
        <>
            <div className="dashbor-content">
                <h1>Latest Movies</h1>
                <div className="row">
                    {daftarMovies !== null && (
                        daftarMovies.map((item, index) => {
                            return (
                                <div className="card" key={index}>
                                    <img src={item.image_url} alt="Hello" />
                                    <div className="card-body">
                                        <Link to={`/detail-movie/${item.id}`}><h2><strong>{item.title}</strong></h2></Link>
                                        <p>{item.genre}</p>
                                        <span style={{ backgroundColor: 'rgb(1, 2, 34)', padding: '5px', float: 'right', color: 'white' }}>{item.rating}/10</span>
                                        <p>{item.year}</p>
                                    </div>
                                </div>
                            )
                        })
                    )}
                </div>
                <h1>Latest Games</h1>
                <div className="row">
                    {daftarGames !== null && (
                        daftarGames.map((item, index) => {
                            return (
                                <div className="card" key={index}>
                                    <img src={item.image_url} alt="Hello" />
                                    <div className="card-body">
                                        <Link to={`/detail-game/${item.id}`}><h2><strong>{item.name}</strong></h2></Link>
                                        <p>{item.genre}</p>
                                        <p>{item.platform}</p>
                                        <p>{item.release}</p>
                                    </div>
                                </div>
                            )
                        })
                    )}
                </div>
            </div>
        </>
    )
}

export default DashborHome