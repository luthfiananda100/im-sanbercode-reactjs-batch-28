import React, { useContext } from "react";
import login from '../Public/Image/login.jpg'
import { Form, Input, Button } from 'antd'
import { contextUser } from "../Context/ContextUser";
import { useHistory } from "react-router";


const Login = () => {
    let history = useHistory();

    const {loginInput, functions} = useContext(contextUser);
    const {handleLogin, handleChange} = functions

    const loginFunc = () =>{
        handleLogin();
        history.push("/dashbor-home")
    }

    return (
        <>
            <div className="home">
                <div className="card-detail-container">
                    <div className="card-detail-img">
                        <img src={login} alt="Login" />
                    </div>
                    <div className="card-detail-detail">
                        <h1 style={{ marginLeft:'160px' }}>Login</h1>
                        <Form
                            style={{ marginTop: '30px' }}
                            name="basic"
                            labelCol={{ span: 5 }}
                            wrapperCol={{ span: 10 }}
                            initialValues={{ remember: true }}
                            onFinish={loginFunc}
                            autoComplete="off"
                        >
                            <Form.Item
                                label="Email"
                                name="email"
                                rules={[{ required: true, message: 'Please input your email!' }]}
                            >
                                <Input name="email" type="text" onChange={handleChange} value={loginInput.email} />
                            </Form.Item>

                            <Form.Item
                                label="Password"
                                name="password"
                                rules={[{ required: true, message: 'Please input your password!' }]}
                            >
                                <Input name="password" type="password" onChange={handleChange} value={loginInput.password} />
                            </Form.Item>

                            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                                <Button type="primary" htmlType="submit">
                                    Login
                                </Button>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Login