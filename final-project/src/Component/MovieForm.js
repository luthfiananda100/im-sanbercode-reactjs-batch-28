import React, { useContext, useEffect } from "react";
import { Form, Input, Button } from "antd";
import { useHistory, useParams } from "react-router";
import { contextPreLogin } from "../Context/ContextItem";

const MovieForm = () => {
    let history = useHistory();

    const { functions, movieInput, setmovieInput, currentIndex, setcurrentIndex, setfetchStatusMovies } = useContext(contextPreLogin)
    const { fetcheditMovies, addMovie, updateMovie } = functions

    let { slug } = useParams();

    const { TextArea } = Input;


    useEffect(() => {
        setmovieInput({
            description: "",
            genre: "",
            title: "",
            duration: 0,
            image_url: "",
            rating: 0,
            review: "",
            year: 0
        })
        if (slug !== undefined) {
            fetcheditMovies(slug)
        }
    }, [slug])

    

    const handleChange = (e) => {
        let typeOfValue = e.target.value
        let name = e.target.name

        setmovieInput({ ...movieInput, [name]: typeOfValue })
    }

    const handleaddMovie = (e) => {
        console.log(e);
        console.log(movieInput);

        if (currentIndex === null) {
            addMovie();
        } else {
            updateMovie(slug);
            setcurrentIndex(null)
        }
        setfetchStatusMovies(true)
        history.push('/dashbor-movie-list')
    }

    return (
        <>
            <div className="dashbor-content">
                <div className="form" style={{ marginRight: 'auto', marginLeft: 'auto', width: '60%', display: 'flex', flexDirection: 'column' }}>
                    <h1 style={{ marginLeft: '20%', marginRight: 'auto' }}>Movie Form</h1>
                    <Form
                        labelCol={{ span: 5 }}
                        wrapperCol={{ span: 15 }}
                        layout="vertical"
                        onFinish={handleaddMovie}
                        autoComplete="off"
                    >
                        <Form.Item
                            label="Title"

                            rules={[{ required: true, message: 'Please input Movie Title' }]}
                        >
                            <Input type="text" name="title" onChange={handleChange} value={movieInput.title} />
                        </Form.Item>
                        <Form.Item
                            label="Genre"

                            rules={[{ required: true, message: 'Please input Movie Genre' }]}
                        >
                            <Input type="text" name="genre" onChange={handleChange} value={movieInput.genre} />
                        </Form.Item>
                        <Form.Item
                            label="Duration"

                            rules={[{ required: true, message: 'Please input Movie Duration' }]}
                        >
                            <Input type="number" name="duration" onChange={handleChange} value={movieInput.duration} />
                        </Form.Item>
                        <Form.Item
                            label="Image Url"

                            rules={[{ required: true, message: 'Please input Movie Image url' }]}
                        >
                            <Input type="text" name="image_url" onChange={handleChange} value={movieInput.image_url} />
                        </Form.Item>
                        <Form.Item
                            label="Rating"

                            rules={[{ required: true, message: 'Please input Movie Rating' }]}
                        >
                            <Input type="number" name="rating" onChange={handleChange} value={movieInput.rating} min={0} max={10} />
                        </Form.Item>
                        <Form.Item
                            label="Review"

                            rules={[{ required: true, message: 'Please input Movie Review' }]}
                        >
                            <Input rows={3} type="text" name="review" onChange={handleChange} value={movieInput.review} />
                        </Form.Item>
                        <Form.Item
                            label="Year"

                            rules={[{ required: true, message: 'Please input Movie Year' }]}
                        >
                            <Input type="number" name="year" onChange={handleChange} value={movieInput.year} />
                        </Form.Item>
                        <Form.Item
                            label="Description"

                            rules={[{ required: true, message: 'Please input Movie Title' }]}
                        >
                            <TextArea rows={10} name="description" type="text" onChange={handleChange} value={movieInput.description} />
                        </Form.Item>
                        <Form.Item wrapperCol={{ offset: 6, span: 16 }}>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </>
    )
}

export default MovieForm