import React, { useContext } from "react";
import './Navbar.css'
import { Link } from "react-router-dom";
import Cookies from "js-cookie";
import { useHistory } from "react-router";
import { contextUser } from "../Context/ContextUser";
import Logo from '../Public/Image/Logo.png'

const Navbar = () => {

    const { setloginStatus } = useContext(contextUser)

    let history = useHistory();

    const handleLogout = () => {
        setloginStatus(false)
        Cookies.remove('user')
        Cookies.remove('email')
        Cookies.remove('token')
        history.push('/login-form')
    }

    return (
        <>
            <div className="navbar">
                <nav>
                    <ul>
                        <Link to="/"><li><img src={Logo} alt="Logo" /></li></Link>
                        <Link to="/"><li>Home</li></Link>
                        <Link to="/movies"><li>Movies</li></Link>
                        <Link to="/games"><li>Games</li></Link>
                    </ul>

                    {
                        Cookies.get('token') !== undefined &&
                        <>
                            <div className="nav-btn">
                                <ul>
                                    <li>
                                        <span onClick={handleLogout}>Logout</span>
                                    </li>
                                </ul>
                            </div>
                        </>
                    }
                    {Cookies.get('token') === undefined && (
                        <>
                            <div className="nav-btn">
                                <ul>
                                    <Link to="/login-form"><li>Login</li></Link>
                                    <Link to="/registration-form"><li>Register</li></Link>
                                </ul>
                            </div>
                        </>
                    )}
                </nav>
            </div>
        </>
    )
}

export default Navbar