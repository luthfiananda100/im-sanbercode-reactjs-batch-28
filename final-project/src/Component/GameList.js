import React, { useContext, useEffect } from "react";
import './Dashbor.css'
import { Table, Button } from 'antd';
import { contextPreLogin } from "../Context/ContextItem";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
import Cookies from "js-cookie";
import { useHistory } from "react-router";

const GameList = () => {
    let history = useHistory();

    const { daftarGames, fetchStatusGames, setfetchStatusGames, functions } = useContext(contextPreLogin);
    const { fetchGames, deleteGame } = functions;

    useEffect(() => {
        if (fetchStatusGames) {
            fetchGames();
            setfetchStatusGames(false)
        }
    }, [fetchStatusGames, setfetchStatusGames, fetchGames])

    const handleDelete = (e) => {
        let deleteIndex = parseInt(e.currentTarget.value)
        let token = Cookies.get('token')
        deleteGame(deleteIndex, token)
    }

    const handleUpdate = (e) => {
        let updateIndex = parseInt(e.currentTarget.value)
        history.push(`/dashbor-game-list-form/${updateIndex}`)
    }

    const columns = [
        {
            title: "Image url",
            dataIndex: 'image_url',
            key: 'url',
            render : (dataIndex) => {
                return(
                    <>
                        {dataIndex.slice(0,40)}...
                    </>
                )
            },
            sorter: (a, b) =>{
                return a.image_url < b.image_url
            }
            
        },
        {
            title: "Name",
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) =>{
                return a.name < b.name
            }
        },
        {
            title: "Genre",
            dataIndex: 'genre',
            key: 'genre',
            sorter: (a, b) =>{
                return a.genre < b.genre
            }

        },
        {
            title: "Release year",
            dataIndex: 'release',
            key: 'release',
            sorter: (a, b) =>{
                return a.release < b.release
            }

        },
        {
            title: "SinglePlayer",
            dataIndex: 'singlePlayer',
            key: 'singlePlayer',
            sorter: (a, b) =>{
                return a.singlePlayer < b.singlePlayer
            }
        },
        {
            title: "Multiplayer",
            dataIndex: 'multiplayer',
            key: 'multiplayer',
            sorter: (a, b) =>{
                return a.multiplayer < b.multiplayer
            }

        },
        {
            title: "Platform",
            dataIndex: 'platform',
            key: 'platform',
            sorter: (a, b) =>{
                return a.duration < b.duration
            }

        },
        {
            title: "Action",
            dataIndex:"id",
            key: 'action',
            render: (dataIndex) => {
                return (
                    <>
                        <Button type="primary" style={{ marginBottom:'5px', width:'100%' }} value={dataIndex} onClick={handleUpdate}><EditOutlined />Edit</Button>
                        <Button type="primary" style={{ width:'100%' }} onClick={handleDelete} value={dataIndex}><DeleteOutlined />Delete</Button>
                    </>
                )
            }
        }
    ]

    return (
        <>
            <div className="dashbor-content">
                <h1 style={{ textAlign: 'center' }}>Game List</h1>
                <Link to="/dashbor-game-list-form"><Button type="primary" style={{ width: '15%', marginBottom: '10px' }}>Add New Game</Button></Link>
                <Table
                    style={{ width:'100%' }}
                    columns={columns}
                    dataSource={daftarGames}
                    pagination={false}
                    >
                </Table>
            </div>
        </>
    )
}

export default GameList