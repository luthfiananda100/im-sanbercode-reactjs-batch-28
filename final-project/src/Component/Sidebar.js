import React from "react";
import { Menu } from 'antd';
import { Link } from "react-router-dom";
import {
    VideoCameraOutlined,
    LockOutlined,
    HomeOutlined,
    PlayCircleOutlined
} from '@ant-design/icons';
import './Sidebar.css'


const Sidebar = () => {
    return (
        <>
            <div className="dashbor-sidebar">
                <Menu theme="light" mode="inline" >
                    <Menu.Item key="1" icon={<HomeOutlined />}>
                        <Link to="/dashbor">Dasbor</Link>
                    </Menu.Item>
                    <Menu.Item key="2" icon={<VideoCameraOutlined />}>
                        <Link to="/dashbor-movie-list">Movies List</Link>
                    </Menu.Item>
                    <Menu.Item key="3" icon={<PlayCircleOutlined />}>
                        <Link to="/dashbor-game-list">Games List</Link>
                    </Menu.Item>
                    <Menu.Item key="4" icon={<LockOutlined />}>
                        <Link to="/dashbor-changepass">Change Password</Link>
                    </Menu.Item>
                </Menu>
            </div>
        </>
    )
}

export default Sidebar