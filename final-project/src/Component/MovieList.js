import React, { useContext, useEffect } from "react";
import './Dashbor.css'
import { Table, Button } from 'antd';
import { contextPreLogin } from "../Context/ContextItem";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
import { useHistory } from "react-router";
import Cookies from "js-cookie";

const MovieList = () => {
    let history = useHistory()

    const { fetchStatusMovies, setfetchStatusMovies, daftarMovies, setcurrentIndex, functions } = useContext(contextPreLogin);
    const { fetchMovies, deleteMovie } = functions;

    useEffect(() => {
        if (fetchStatusMovies) {
            fetchMovies()
        }
        setfetchStatusMovies(false)
    })

    

    const handleDelete = (e) => {
        let indexDelete = parseInt(e.currentTarget.value)
        let token = Cookies.get('token')
        deleteMovie(indexDelete, token);

    }

    const handleEdit = (e) => {
        let indexEdit = parseInt(e.currentTarget.value)
        setcurrentIndex(indexEdit)
        history.push(`/dashbor-movie-list-form/${indexEdit}`)
    }

    const columns = [
        {
            title: "Image url",
            dataIndex: 'image_url',
            key: 'url',
            render : (dataIndex) => {
                return(
                    <>
                        {dataIndex.slice(0,20)}...
                    </>
                )
            },
            sorter: (a, b) => {
                return a.image_url < b.image_url
            }

        },
        {
            title: "Title",
            dataIndex: 'title',
            key: 'title',
            render : (dataIndex) => {
                return(
                    <>
                        {dataIndex.slice(0,50)}...
                    </>
                )
            },
            sorter: (a, b) => {
                return a.title < b.title
            }
        },
        {
            title: "Year",
            dataIndex: 'year',
            key: 'year',
            sorter: (a, b) => {
                return a.year < b.year
            }

        },
        {
            title: "Rating",
            dataIndex: 'rating',
            key: 'rating',
            sorter: (a, b) => {
                return a.rating < b.rating
            }
        },
        {
            title: "Genre",
            dataIndex: 'genre',
            key: 'genre',
            render : (dataIndex) => {
                return(
                    <>
                        {dataIndex.slice(0,50)}...
                    </>
                )
            },
            sorter: (a, b) => {
                return a.genre < b.genre
            }

        },
        {
            title: "Duration",
            dataIndex: 'duration',
            key: 'duration',
            sorter: (a, b) => {
                return a.duration < b.duration
            }

        },
        {
            title: "Review",
            dataIndex: 'review',
            key: 'review',
            render : (dataIndex) => {
                return(
                    <>
                        {dataIndex.slice(0,50)}...
                    </>
                )
            },
            sorter: (a, b) => {
                return a.review < b.review
            }

        },
        {
            title: "Description",
            dataIndex: 'description',
            key: 'description',
            render: (dataIndex) => {
                return (
                    <>
                        {dataIndex.slice(0,50)}...
                    </>
                )
            },
            sorter: (a, b) => {
                return a.description < b.description
            }

        },
        {
            title: "Action",
            key: 'action',
            dataIndex: 'id',
            render: (dataIndex) => {
                return (
                    <>
                        <Button type="primary" value={dataIndex} style={{ marginBottom: '5px', width: '100%' }} onClick={handleEdit}><EditOutlined />Edit</Button>
                        <Button type="primary" value={dataIndex} style={{ width: '100%' }} onClick={handleDelete}><DeleteOutlined />Delete</Button>
                    </>
                )
            }
        }
    ]

    return (
        <>
            <div className="dashbor-content">
                <h1 style={{ textAlign: 'center' }}>Movie List</h1>
                <Link to="/dashbor-movie-list-form"><Button type="primary" style={{ width: '15%', marginBottom: '10px' }}>Add New Movie</Button></Link>
                <Table
                    tableLayout='fixed'
                    columns={columns}
                    dataSource={daftarMovies}
                    pagination={false}
                >
                </Table>
            </div>
        </>
    )
}

export default MovieList