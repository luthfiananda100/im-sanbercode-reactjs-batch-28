// Layout After Login

import React from "react";
import Footer from "../../Component/Footer";
import Navbar from "../../Component/Navbar";
import Sidebar from "../../Component/Sidebar";
import './LayoutAfter.css'

const LayoutAfter = (props) => {
    return (
        <>
            <div className="LaoutAfter">
                <Navbar />
                <div className="dashbor-class">
                    <Sidebar />
                    {props.content}
                </div>
                <Footer />
            </div>
        </>
    )
}

export default LayoutAfter;