// Layout Before Login

import React from "react";
import Footer from "../../Component/Footer";
import Navbar from "../../Component/Navbar";
import './LayoutBefore.css'

const LayoutBefore = (props) => {
    return (
        <>

            <div className="LayoutBefore">
                <Navbar />
                {props.content}
                <Footer />
            </div>

        </>
    )
}

export default LayoutBefore