import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom'

import LayoutBefore from "./Layouts/BeforeLogin/LayoutsBefore";
import Home from "./Component/Home";
import HomeMovies from "./Component/HomeMovies";
import HomeGames from "./Component/HomeGames";
import DetailMovie from "./Component/DetailMovie";
import DetailGames from "./Component/DetailGames";
import Registration from "./Component/Registration";
import Login from "./Component/Login";
import Cookies from "js-cookie";
import { Redirect } from "react-router";
import LayoutAfter from "./Layouts/AfterLogin/LayoutAfter";
import Dashbor from "./Component/Dashbor";
import DashborHome from "./Component/DashborHome";
import { UserProvider } from "./Context/ContextUser";
import { PreLoginProvider } from "./Context/ContextItem";
import DashborMovies from "./Component/DashborMovies";
import DashborGames from "./Component/DashborGames";
import MovieList from "./Component/MovieList";
import GameList from "./Component/GameList";
import MovieForm from "./Component/MovieForm";
import GameForm from "./Component/GameForm";
import ChangePass from "./Component/ChangePass";


const Routes = () => {

    const LoginRoute = ({ ...props }) => {
        if (Cookies.get('token') !== undefined) {
            return <Redirect to="/dashbor-home" />
        } else {
            return <Route {...props} />
        }
    }

    const HomeRoute = ({ ...props }) => {
        if (Cookies.get('token') !== undefined) {
            return <Redirect to="/dashbor-home" />
        } else {
            return <Route {...props} />
        }
    }

    const MovieRoute = ({ ...props }) => {
        if (Cookies.get('token') !== undefined) {
            return <Redirect to="/dashbor-movies" />
        } else {
            return <Route {...props} />
        }
    }

    const GameRoute = ({ ...props }) => {
        if (Cookies.get('token') !== undefined) {
            return <Redirect to="/dashbor-games" />
        } else {
            return <Route {...props} />
        }
    }

    return (
        <>
            <PreLoginProvider>
                <UserProvider>
                    <Router>
                        <Switch>
                            <HomeRoute path="/" exact>
                                <LayoutBefore content={<Home />} />
                            </HomeRoute>
                            <MovieRoute path="/movies" exact>
                                <LayoutBefore content={<HomeMovies />} />
                            </MovieRoute>
                            <GameRoute path="/games" exact>
                                <LayoutBefore content={<HomeGames />} />
                            </GameRoute>
                            <Route path="/detail-movie/:slug" exact>
                                <LayoutBefore content={<DetailMovie />} />
                            </Route>
                            <Route path="/detail-game/:slug" exact>
                                <LayoutBefore content={<DetailGames />} />
                            </Route>
                            <LoginRoute path="/registration-form" exact>
                                <LayoutBefore content={<Registration />} />
                            </LoginRoute>
                            <LoginRoute path="/login-form" exact>
                                <LayoutBefore content={<Login />} />
                            </LoginRoute>
                            <Route path="/dashbor" exact>
                                <LayoutAfter content={<Dashbor />} />
                            </Route>
                            <Route path="/dashbor-home" exact>
                                <LayoutAfter content={<DashborHome />} />
                            </Route>
                            <Route path="/dashbor-movies" exact>
                                <LayoutAfter content={<DashborMovies />} />
                            </Route>
                            <Route path="/dashbor-games" exact>
                                <LayoutAfter content={<DashborGames />} />
                            </Route>
                            <Route path="/dashbor-movie-list" exact>
                                <LayoutAfter content={<MovieList />} />
                            </Route>
                            <Route path="/dashbor-game-list" exact>
                                <LayoutAfter content={<GameList />} />
                            </Route>
                            <Route path="/dashbor-movie-list-form" exact>
                                <LayoutAfter content={<MovieForm />} />
                            </Route>
                            <Route path="/dashbor-movie-list-form/:slug" exact>
                                <LayoutAfter content={<MovieForm />} />
                            </Route>
                            <Route path="/dashbor-game-list-form" exact>
                                <LayoutAfter content={<GameForm />} />
                            </Route>
                            <Route path="/dashbor-game-list-form/:slug" exact>
                                <LayoutAfter content={<GameForm />} />
                            </Route>
                            <Route path="/dashbor-changepass" exact>
                                <LayoutAfter content={<ChangePass />} />
                            </Route>
                        </Switch>
                    </Router>
                </UserProvider>
            </PreLoginProvider>
        </>
    )
}

export default Routes