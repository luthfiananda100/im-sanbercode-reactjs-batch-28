import React, { createContext, useState } from "react";
import axios from "axios";
import Cookies from "js-cookie";
import { message } from "antd";

export const contextPreLogin = createContext();

export const PreLoginProvider = (props) => {
    const [currentIndex, setcurrentIndex] = useState(null)

    const [fetchStatus, setfetchStatus] = useState(true);

    // For Movies Page
    const [fetchStatusMovies, setfetchStatusMovies] = useState(true);
    const [daftarMovies, setdaftarMovies] = useState([]);

    // For Games Page
    const [daftarGames, setdaftarGames] = useState([]);
    const [fetchStatusGames, setfetchStatusGames] = useState(true);

    const [gameInput, setgameInput] = useState({
        name: "",
        genre: "",
        singlePlayer: false,
        multiplayer: false,
        platform: "",
        release: 0,
        image_url: ""
    })

    // For Detail Movie
    const [datamovieById, setdatamovieById] = useState({
        description: "",
        genre: "",
        id: 0,
        title: "",
        duration: 0,
        image_url: "",
        rating: 0,
        review: "",
        year: 0
    });

    // For Detail Game
    const [datagameById, setdatagameById] = useState({
        id: 0,
        name: "",
        genre: "",
        singlePlayer: 0,
        multiplayer: 0,
        platform: "",
        release: 0,
        image_url: ""
    })


    const [movieInput, setmovieInput] = useState({
        description: "",
        genre: "",
        title: "",
        duration: 0,
        image_url: "",
        rating: 0,
        review: "",
        year: 0
    })

    // Add Movie
    const addMovie = () => {
        axios.post(`https://backendexample.sanbersy.com/api/data-movie`,
            movieInput,
            { headers: { "Authorization": "Bearer " + Cookies.get('token') } }
        )
            .then(res => {
                var dataResult = res.data
                setdaftarMovies([...daftarMovies, {
                    description: dataResult.description,
                    genre: dataResult.genre,
                    id: dataResult.id,
                    title: dataResult.title,
                    rating: dataResult.rating,
                    image_url: dataResult.image_url,
                    review: dataResult.review,
                    year: dataResult.year,
                    duration: dataResult.duration,
                    key: dataResult.id
                }])
                message.success("Movie Added")
            })
    }


    // Function Get Movies
    const fetchMovies = async () => {
        let movies = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        let moviesResult = movies.data

        setdaftarMovies(moviesResult.map((item) => {
            return {
                description: item.description,
                genre: item.genre,
                id: item.id,
                title: item.title,
                rating: item.rating,
                image_url: item.image_url,
                review: item.review,
                year: item.year,
                duration: item.duration,
                key: item.id
            }
        }))
    }


    // Get Movie By Id
    const fetchMoviesById = (id) => {
        axios.get(`https://backendexample.sanbersy.com/api/data-movie/${id}`)
            .then((res) => {
                let result = res.data
                setdatamovieById({
                    description: result.description,
                    genre: result.genre,
                    id: result.id,
                    title: result.title,
                    rating: result.rating,
                    image_url: result.image_url,
                    review: result.review,
                    year: result.year,
                    duration: result.duration,
                    key: result.id
                })
            })
    }

    // Edit Movie
    const fetcheditMovies = (id) => {
        axios.get(`https://backendexample.sanbersy.com/api/data-movie/${id}`)
            .then((res) => {
                let result = res.data
                setmovieInput({
                    description: result.description,
                    genre: result.genre,
                    title: result.title,
                    rating: result.rating,
                    image_url: result.image_url,
                    review: result.review,
                    year: result.year,
                    duration: result.duration
                })
            })
    }

    //Update Movie
    const updateMovie = (id) => {
        axios.put(`https://backendexample.sanbersy.com/api/data-movie/${id}`, movieInput,
            { headers: { "Authorization": "Bearer " + Cookies.get('token') } }
        ).then((res) => {
            setfetchStatusMovies(true)
            message.success("Data Movie Updated")
        })
    }

    //Delete Movie
    const deleteMovie = (indexDelete, token) => {
        axios.delete(`https://backendexample.sanbersy.com/api/data-movie/${indexDelete}`,
            { headers: { "Authorization": "Bearer " + token } }
        )
            .then(() => {
                setfetchStatusMovies(true)
                message.success("Data Deleted")
            })
    }

    // Add Game
    const addGame = () => {
        axios.post(`https://backendexample.sanbersy.com/api/data-game`, gameInput,
            { headers: { "Authorization": "Bearer " + Cookies.get('token') } }
        ).then(res => {
            var dataResult = res.data
            setdaftarGames([...daftarGames, {
                singlePlayer: dataResult.singlePlayer,
                genre: dataResult.genre,
                id: dataResult.id,
                name: dataResult.name,
                multiplayer: dataResult.multiplayer,
                image_url: dataResult.image_url,
                platform: dataResult.platform,
                release: dataResult.release,
                key: dataResult.id
            }])
            setfetchStatusGames(true);
            message.success("Game Added")
        })
    }

    // Get Game By Id
    const fetchGamesById = (id) => {
        axios.get(`https://backendexample.sanbersy.com/api/data-game/${id}`)
            .then((res) => {
                let result = res.data
                console.log(result);
                setdatagameById({
                    name: result.name,
                    genre: result.genre,
                    id: result.id,
                    image_url: result.image_url,
                    release: result.release,
                    singlePlayer: result.singlePlayer,
                    multiplayer: result.multiplayer,
                    platform: result.platform
                })
            })
    }

    // Edit Game
    const editGame = (id) => {
        axios.get(`https://backendexample.sanbersy.com/api/data-game/${id}`)
            .then((res) => {
                let result = res.data
                setgameInput({
                    name: result.name,
                    genre: result.genre,
                    id: result.id,
                    image_url: result.image_url,
                    release: result.release,
                    singlePlayer: result.singlePlayer,
                    multiplayer: result.multiplayer,
                    platform: result.platform,
                    key:result.id,
                    nama:result.name
                })
            })
    }

    //Update Game
    const updateGame = (id) => {
        axios.put(`https://backendexample.sanbersy.com/api/data-game/${id}`, gameInput,
            { headers: { "Authorization": "Bearer " + Cookies.get('token') } }
        ).then((res) => {
            setfetchStatusGames(true)
            message.success("Data Game Updated")
        })
    }

    //Delete Game
    const deleteGame = (indexDelete, token) => {
        axios.delete(`https://backendexample.sanbersy.com/api/data-game/${indexDelete}`,
            { headers: { "Authorization": "Bearer " + token } }
        )
            .then(() => {
                setfetchStatusGames(true)
                message.success("Data Deleted")
            })
    }

    // Function Get All Games
    const fetchGames = async () => {
        let games = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        let gamesResult = games.data

        setdaftarGames(gamesResult.map((item) => {
            return {
                name: item.name,
                genre: item.genre,
                id: item.id,
                image_url: item.image_url,
                release: item.release,
                singlePlayer: item.singlePlayer,
                multiplayer: item.multiplayer,
                platform: item.platform,
                key: item.id
            }
        }))
    }

    let functions = {
        fetchMovies,
        fetchGames,
        fetchMoviesById,
        fetcheditMovies,
        fetchGamesById,
        addMovie,
        updateMovie,
        addGame,
        deleteMovie,
        deleteGame,
        editGame,
        updateGame
    }

    return (
        <contextPreLogin.Provider value={{
            fetchStatus,
            setfetchStatus,
            daftarMovies,
            setdaftarMovies,
            daftarGames,
            setdaftarGames,
            fetchStatusMovies,
            setfetchStatusMovies,
            fetchStatusGames,
            setfetchStatusGames,
            datamovieById,
            setdatamovieById,
            datagameById,
            setdatagameById,
            movieInput,
            setmovieInput,
            currentIndex,
            setcurrentIndex,
            gameInput,
            setgameInput,
            functions
        }}>
            {props.children}
        </contextPreLogin.Provider>
    )
}