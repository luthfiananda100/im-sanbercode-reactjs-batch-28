import React, { createContext, useState } from "react";
import axios from "axios";
import Cookies from "js-cookie";
import { message } from "antd";

export const contextUser = createContext();

export const UserProvider = (props) => {
    const [changepassInput, setchangepassInput] = useState({
        current_password: "",
        new_password: "",
        new_confirm_password: ""
    })

    const [loginInput, setloginInput] = useState({
        email: "",
        password: ""
    })
    const [loginStatus, setloginStatus] = useState(false)

    const [regisInput, setregisInput] = useState({
        name: "",
        email: "",
        password: ""
    })

    const handleLogin = () => {
        axios.post(`https://backendexample.sanbersy.com/api/user-login`, {
            email: loginInput.email,
            password: loginInput.password
        }).then((res) => {
            var user = res.data.user
            var token = res.data.token

            Cookies.set('user', user.name, { expires: 1 })
            Cookies.set('email', user.email, { expires: 1 })
            Cookies.set('token', token, { expires: 1 })
            setloginStatus(true)
        })
    }

    const handleChange = (event) => {
        let typeOfValue = event.target.value
        let name = event.target.name

        setloginInput({ ...loginInput, [name]: typeOfValue })
        console.log(loginInput);
    }

    const regisSubmit = () => {
        axios.post("https://backendexample.sanbersy.com/api/register", {
            name: regisInput.name,
            email: regisInput.email,
            password: regisInput.password
        }).then(() => {
            message.success('Account Registered');
        }
        ).catch((err) => {
            alert(err)
        })
    }

    const changePassword = (token) => {
        axios.post(`https://backendexample.sanbersy.com/api/change-password`, changepassInput,
            { headers: { "Authorization": "Bearer " + token } }
        ).then(() => {
            message.success('Password Changed');
            setchangepassInput({
                current_password: "",
                new_password: "",
                new_confirm_password: ""
            })
        }
        ).catch((err) => {
            alert(err)
        })
    }

    const functions = {
        handleLogin,
        handleChange,
        regisSubmit,
        changePassword
    }

    return (
        <contextUser.Provider value={{
            loginInput,
            setloginInput,
            loginStatus,
            setloginStatus,
            regisInput,
            setregisInput,
            functions,
            changepassInput,
            setchangepassInput
        }}>
            {props.children}
        </contextUser.Provider>
    )
}
