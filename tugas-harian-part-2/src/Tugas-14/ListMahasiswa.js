import React, {useContext, useEffect} from "react";
import { contextMhs } from "../Context/ContextMhs";
import {Link, useHistory} from "react-router-dom";

const ListMahasiswa = () =>{
    const { daftarMhs, functions, fetchStatus, setfetchStatus } = useContext(contextMhs)

    const { fetchData, deleteMhs, showIndeks } = functions;

    let history = useHistory();

    useEffect(() => {
        if(fetchStatus===false){
            fetchData()
            setfetchStatus(true);
        }
    }, [fetchData, fetchStatus, setfetchStatus])


    const handleEdit = (event) => {
        let editIndex = parseInt(event.target.value);
        // edit(editIndex);
        history.push(`tugas14/form/${editIndex}`)
        
    }

    const handleDelete = (event) => {
        let delIndex = parseInt(event.target.value);
        deleteMhs(delIndex);
    }

    return (
        <>
            <div className="Daftar-nilai">
                <h1>Daftar Nilai Mahasiswa</h1>
                <Link to="/tugas14/form"> <button>Buat Mahasiswa Baru</button></Link>
                <table>
                    <thead>
                        <tr>
                            <td>No</td>
                            <td>Nama</td>
                            <td>Mata Kuliah</td>
                            <td>Nilai</td>
                            <td>Indeks Nilai</td>
                            <td>Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            daftarMhs.map((item, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{item.name}</td>
                                        <td>{item.course}</td>
                                        <td>{item.score}</td>
                                        <td>{showIndeks(item.score)}</td>
                                        <td><button onClick={handleEdit} value={item.id}>Edit</button>
                                            <button onClick={handleDelete} value={item.id}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
            
        </>
    )
}

export default ListMahasiswa;