import Tugas9 from '../Tugas-9/tugas9'
import Tugas10 from '../Tugas-10/tugas10'
import Tugas11 from '../Tugas-11/Tugas11'
import Tugas12 from '../Tugas-12/Tugas12'
import DaftarMahasiswa from '../Tugas-13/DaftarMahasiswa';
import Nav from './Nav'
import FormMhs from "./FormMhs";
// import ButtonTheme from '../Component/ButtonTheme';

import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import { ThemeProvider } from '../Context/ThemeContext';
import { MahasiswaProvider } from '../Context/ContextMhs'
import ListMahasiswa from './ListMahasiswa';
import FormAnt from '../Tugas-15/FormAnt';
import ListAnt from '../Tugas-15/ListAnt';

const Routes = () => {
    return (
        <>
            <Router>
                <MahasiswaProvider>
                    <ThemeProvider>
                        <Nav />
                        {/* <ButtonTheme /> */}
                        <Switch>
                            <Route path="/" exact component={Tugas9} />
                            <Route path="/tugas9" exact component={Tugas9} />
                            <Route path="/tugas10" exact component={Tugas10} />
                            <Route path="/tugas11" exact component={Tugas11} />
                            <Route path="/tugas12" exact component={Tugas12} />
                            <Route path="/tugas13" exact component={DaftarMahasiswa} />
                            <Route path="/tugas14" exact >
                                <ListMahasiswa />
                            </Route>
                            <Route path="/tugas14/form" exact component={FormMhs} />
                            <Route path="/tugas14/form/:slug" exact component={FormMhs} />

                            <Route path="/tugas15" exact >
                                <ListAnt />
                            </Route>
                            <Route path="/tugas15/form" exact component={FormAnt} />
                            <Route path="/tugas15/form/:slug" exact component={FormAnt} />
                        </Switch>
                    </ThemeProvider>
                </MahasiswaProvider>
            </Router>
        </>
    )
}

export default Routes;
