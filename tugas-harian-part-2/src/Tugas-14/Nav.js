import React, { useContext } from 'react'
import { Link } from "react-router-dom";
import '../App.css'
import { ThemeContext } from '../Context/ThemeContext';
// import ButtonTheme from '../Component/ButtonTheme';
import SwitchTheme from '../Component/SwitchTheme';


const Nav = () => {
    const { theme } = useContext(ThemeContext);

    const navTheme = theme === "light" ? "light-nav" : "dark-nav"

    return (
        <>
            <div className={navTheme}>
                <ul>
                    <Link to="/tugas9"><li>Tugas 9</li></Link>
                    <Link to="/tugas10"><li>Tugas 10</li></Link>
                    <Link to="/tugas11"><li>Tugas 11</li></Link>
                    <Link to="/tugas12"><li>Tugas 12</li></Link>
                    <Link to="/tugas13"><li>Tugas 13</li></Link>
                    <Link to="/tugas14"><li>Tugas 14</li></Link>
                    <Link to="/tugas15"><li>Tugas 15</li></Link>
                    <SwitchTheme/>
                </ul>
            </div>
        </>
    )
}

export default Nav;