import React, {useContext, useEffect} from "react";
import { contextMhs } from "../Context/ContextMhs";
import {Link, useHistory} from "react-router-dom";
import { Button, message } from 'antd';
import 'antd/dist/antd.css';
import {
    DeleteOutlined,
    EditOutlined
  } from '@ant-design/icons';

const ListAnt = () =>{
    const { daftarMhs, functions, fetchStatus, setfetchStatus } = useContext(contextMhs)

    const { fetchData, deleteMhs, showIndeks } = functions;

    let history = useHistory();

    useEffect(() => {
        if(fetchStatus===false){
            fetchData()
            setfetchStatus(true);
            // message.success("Data Berhasil Ditambah")
        }
    }, [fetchData, fetchStatus, setfetchStatus])


    const editHandler = (event) => {
        let editIndex = parseInt(event.target.value);
        // edit(editIndex);
        history.push(`tugas15/form/${editIndex}`)
        
    }

    const deleteHandler = (event) => {
        let delIndex = parseInt(event.target.value);
        deleteMhs(delIndex);
        setTimeout(() => {
            message.success('Data Berhasil Dihapus')
          }, 1000);
    }

    return (
        <>
            <div className="Daftar-nilai-ant">
                <h1>Daftar Nilai Mahasiswa</h1>
                <Link to="/tugas15/form"> <Button type="primary" style={{ marginBottom:'10px' }}>Buat Mahasiswa Baru</Button></Link>
                <table>
                    <thead>
                        <tr>
                            <td>No</td>
                            <td>Nama</td>
                            <td>Mata Kuliah</td>
                            <td>Nilai</td>
                            <td>Indeks Nilai</td>
                            <td>Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            daftarMhs.map((item, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{item.name}</td>
                                        <td>{item.course}</td>
                                        <td>{item.score}</td>
                                        <td>{showIndeks(item.score)}</td>
                                        <td><Button type="primary" style={{ backgroundColor:'yellow', borderColor:'yellow' }} onClick={editHandler} value={item.id}><EditOutlined /></Button>
                                            <Button type="primary" style={{ backgroundColor:'red', borderColor:'red' }} onClick={deleteHandler} value={item.id}><DeleteOutlined /></Button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
            
        </>
    )
}

export default ListAnt;