import React, { useContext, useEffect } from 'react'
import { contextMhs } from "../Context/ContextMhs";
import {Link, useHistory, useParams} from 'react-router-dom';
import './Tugas15.css'
import {message} from 'antd'

const FormAnt = () =>{
    const { formName, setformName, formCourse, setformCourse, formScore,setformScore, currentIndex, setcurrentIndex, functions } = useContext(contextMhs)

    const { submitMhs, updateMhs, fetchbyId } = functions;

    let history = useHistory();

    let {slug} = useParams();

    useEffect(() =>{
        if(slug !== undefined){
            fetchbyId(slug)
        }
    }, [])

    // let history = useHistory();
    const handleSubmit = (event) => {
        event.preventDefault();

        let newData = { id: null, created_at: null, updated_at: null, name: formName, course: formCourse, score: formScore };

        // Input Biasa
        if (currentIndex === null) {
            submitMhs(newData);
        } else {              // Update
            updateMhs(currentIndex);
        }
        setformName("");
        setformCourse("");
        setformScore("");
        setcurrentIndex(null);
        history.push("/tugas15");
        setTimeout(() => {
            message.success('Data Berhasil Ditambahkan')
          }, 1000);

    }

    const nameChange = (event) => {
        setformName(event.target.value);
    }

    const courseChange = (event) => {
        setformCourse(event.target.value);
    }

    const scoreChange = (event) => {
        setformScore(event.target.value);
    }

    return (
        <div className="Form-nilai-container">
                <h1>Form Nilai Mahasiswa</h1>
                <form onSubmit={handleSubmit}>
                    <table>
                        <tbody>
                            <tr>
                                <td className="table-key"><label htmlFor="inputName">Nama :</label></td>
                                <td className="table-val"><input type="text" name="inputName" id="inputName" value={formName} onChange={nameChange} /></td>
                            </tr>
                            <tr>
                                <td className="table-key"><label htmlFor="inputName">Mata Kuliah : </label></td>
                                <td className="table-val"><input type="text" name="inputName" id="inputName" value={formCourse} onChange={courseChange} /></td>
                            </tr>
                            <tr>
                                <td className="table-key"><label htmlFor="inputName">Nilai</label></td>
                                <td className="table-val"><input type="number" name="inputName" id="inputName" placeholder="0" value={formScore} onChange={scoreChange} min="0" max="100" /></td>
                            </tr>
                            <tr>
                                <td><Link to="/tugas15">Kembali Ke List</Link></td>
                                <td className="table-val"><button>Submit</button></td>
                            </tr>

                        </tbody>
                    </table>
                </form>
            </div>
    )
}

export default FormAnt
