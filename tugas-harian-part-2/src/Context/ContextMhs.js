import React, { createContext, useState } from 'react'
import axios from 'axios';

export const contextMhs = createContext();

export const MahasiswaProvider = props => {
    const [daftarMhs, setdaftarMhs] = useState([]);
    const [formName, setformName] = useState("");
    const [formCourse, setformCourse] = useState("");
    const [formScore, setformScore] = useState("");
    const [currentIndex, setcurrentIndex] = useState(null);
    const [fetchStatus, setfetchStatus] = useState(false);
    const [redirectStat, setRedirectStat] = useState(false);

    const fetchData = async () => {
        const result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)

        setdaftarMhs(result.data.map(x => { return { id: x.id, created_at: x.created_at, updated_at: x.updated_at, name: x.name, course: x.course, score: x.score } }))
    }


    const submitMhs = (newData) => {
        axios.post('http://backendexample.sanbercloud.com/api/student-scores', newData)
            .then(res => {
                let data = res.data;
                setdaftarMhs([...daftarMhs, { id: data.id, created_at: data.created_at, updated_at: data.updated_at, name: data.name, course: data.course, score: data.score }]);
                console.log(daftarMhs);
            })
    }

    const deleteMhs = (id) => {
        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${id}`)
            .then(() => {
                let newData = daftarMhs.filter(e => { return e.id !== id })
                setdaftarMhs(newData);
            })
    }

    const updateMhs = (id) => {
        axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${id}`,{
            name:formName,
            course:formCourse,
            score:formScore
        }).then((res) =>{
            setfetchStatus(false)
            console.log(res)
        })
    }

    const edit = (id) => {
        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${id}`)
            .then(res => {
                let data = res.data;
                setformName(data.name);
                setformCourse(data.course);
                setformScore(data.score);
            })
        setcurrentIndex(id);
    }

    const fetchbyId = (id) => {
        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${id}`)
            .then(res => {
                let data = res.data;
                setformName(data.name);
                setformCourse(data.course);
                setformScore(data.score);
            })
        setcurrentIndex(id);
    }

    const showIndeks = (nilai) => {
        if (nilai > 80) {
            return 'A'
        } else if (nilai < 80 && nilai >= 70) {
            return 'B'
        } else if (nilai < 70 && nilai >= 60) {
            return 'C'
        } else if (nilai < 60 && nilai >= 50) {
            return 'D'
        } else {
            return 'E'
        }
    }

    const functions = {
        fetchData,
        submitMhs,
        deleteMhs,
        updateMhs,
        edit,
        showIndeks,
        fetchbyId
    }

    return (
        <contextMhs.Provider value={{
            daftarMhs,
            setdaftarMhs,
            formName,
            setformName,
            formCourse,
            setformCourse,
            formScore,
            setformScore,
            currentIndex,
            setcurrentIndex,
            functions,
            fetchStatus,
            setfetchStatus,
            redirectStat,
            setRedirectStat
        }}>
            {props.children}
        </contextMhs.Provider>
    )
}