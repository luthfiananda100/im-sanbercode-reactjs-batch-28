import React, { useContext } from "react";
import { ThemeContext } from "../Context/ThemeContext";

const ButtonTheme = () =>{
    const {theme, setTheme} = useContext(ThemeContext);

    const handleClick = () =>{
        setTheme(theme === 'light' ? 'dark' : 'light')
    }

    return (
        <button style={{borderRadius:'10px', marginLeft:'auto', marginRight:'20px'}} onClick={handleClick}>Change Theme</button>
    )
}

export default ButtonTheme