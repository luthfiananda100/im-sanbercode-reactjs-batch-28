import { Switch } from 'antd';
import React, { useContext } from "react";
import { ThemeContext } from "../Context/ThemeContext";

const SwitchTheme = () => {
    const { theme, setTheme } = useContext(ThemeContext);
    function onChange(checked) {
        console.log(`switch to ${checked}`);
        setTheme(theme === 'light' ? 'dark' : 'light')
    }
    return (
        <Switch checkedChildren="Dark" unCheckedChildren="Light" defaultChecked onChange={onChange} 
        style={{ marginLeft:'auto', marginTop:'auto', marginBottom:'auto', marginRight:'20px' }}
        />
    )
}

export default SwitchTheme;