import React, { useState, useEffect } from 'react'


function Tugas10(){
    const [counter, setCounter] = useState(100);
    const [currentTime, setCurrentTime] = useState("");
    const getTime = ()=>{
        var date = new Date();
        var hours = date.getHours();
        if(parseInt(hours)<10) hours = "0" + hours;
        var mins = date.getMinutes();
        if(parseInt(mins)<10) mins = "0" + mins;
        var secs = date.getSeconds();
        if(parseInt(secs)<10) secs = "0" + secs;

        var desc = "PM";
        if(parseInt(hours)<=12){
            desc = "AM";
        }

        var time = hours + ":" + mins + ":" + secs + " " + desc;
        setCurrentTime(time);
    }


    useEffect(() => {
        if(counter===0){
            return;
        }
        console.log("Halo");
        var interv = setInterval(() => {
            getTime();
            setCounter(counter-1);
        }, 1000);
        return function cleanup(){
            clearInterval(interv)
        }
    },[counter])

    if(counter<=0){
        return null
    }
    return(
        <div className="ClockActive">
            <p>Now At - {currentTime}</p>
            <small>Countdown : {counter}</small>
        </div>
    )
}

export default Tugas10;