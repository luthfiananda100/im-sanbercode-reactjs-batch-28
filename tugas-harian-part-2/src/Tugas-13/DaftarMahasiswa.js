import React from 'react'
import { MahasiswaProvider } from '../Context/ContextMhs'
import Tugas13 from './Tugas13'
import MahasiswaForm from './MahasiswaForm'

const DaftarMahasiswa = () =>{
    return (
        <MahasiswaProvider>
            <Tugas13 />
            <MahasiswaForm/>
        </MahasiswaProvider>
    )
}

export default DaftarMahasiswa;