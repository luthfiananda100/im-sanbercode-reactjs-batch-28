import React, { useEffect, useContext } from "react";
import './Tugas13.css'
import { contextMhs } from "../Context/ContextMhs";

function Tugas13() {
    const { daftarMhs, functions, fetchStatus, setfetchStatus } = useContext(contextMhs)

    const { fetchData, deleteMhs, edit, showIndeks } = functions;

    useEffect(() => {
        if(fetchStatus===false){
            fetchData()
            setfetchStatus(true);
        }
    })


    const handleEdit = (event) => {
        let editIndex = parseInt(event.target.value);
        edit(editIndex);
        
    }

    const handleDelete = (event) => {
        let delIndex = parseInt(event.target.value);
        deleteMhs(delIndex);
    }

    return (
        <>
            <div className="Daftar-nilai">
                <h1>Daftar Nilai Mahasiswa</h1>
                <table>
                    <thead>
                        <tr>
                            <td>No</td>
                            <td>Nama</td>
                            <td>Mata Kuliah</td>
                            <td>Nilai</td>
                            <td>Indeks Nilai</td>
                            <td>Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            daftarMhs.map((item, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{item.name}</td>
                                        <td>{item.course}</td>
                                        <td>{item.score}</td>
                                        <td>{showIndeks(item.score)}</td>
                                        <td><button onClick={handleEdit} value={item.id}>Edit</button>
                                            <button onClick={handleDelete} value={item.id}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
            
        </>
    )
}

export default Tugas13;