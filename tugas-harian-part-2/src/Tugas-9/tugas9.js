import React from 'react';
import logo from '../logo.png';
import '../App.css'

function Tugas9(){
    function Materi(props) {
        return <label htmlFor="">Belajar {props.name}</label>
      }
      
      return (
        <div className="App">
          <body>
            <div className="Main">
              <img src={logo} alt="Your" />
              <p style={{ fontSize: 40, fontWeight: 'lighter', marginBottom: 0, marginTop: 35, color: 'gray' }}>THINGS TO DO</p>
              <p style={{ color: 'gray', marginTop: 0, marginRight: 10 }}>During bootcamp in sanbercode</p><hr />
              <input type="checkbox" name="git" id="git" value="" />
              <Materi name="GIT & CLI" /><hr />
              <input type="checkbox" name="html" id="html" value="" />
              <Materi name="HTML & CSS" /><hr />
              <input type="checkbox" name="js" id="js" value="" />
              <Materi name="Javascript" /><hr />
              <input type="checkbox" name="react" id="react" value="" />
              <Materi name="ReactJS Dasar" /><hr />
              <input type="checkbox" name="advance" id="advance" value="" />
              <Materi name="ReactJS Advance" /><hr />
              <button type="button" className="tugas9-btn">Send</button>
            </div>
          </body>
        </div>
      )
}

export default Tugas9;