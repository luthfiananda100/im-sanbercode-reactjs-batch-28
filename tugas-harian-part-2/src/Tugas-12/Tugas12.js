import React, { useState, useEffect } from "react";
import './Tugas12.css'
import axios from "axios";

function Tugas12() {
    const [daftarMhs, setdaftarMhs] = useState([]);

    const [formName, setformName] = useState("");
    const [formCourse, setformCourse] = useState("");
    const [formScore, setformScore] = useState("");
    const [currentIndex, setcurrentIndex] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)

            setdaftarMhs(result.data.map(x => { return { id: x.id, created_at: x.created_at, updated_at: x.updated_at, name: x.name, course: x.course, score: x.score } }))
        }

        fetchData()
    }, [])

    const handleSubmit = (event) => {
        event.preventDefault();

        let newData = { id: null, created_at: null, updated_at: null, name: formName, course: formCourse, score: formScore };

        // Input Biasa
        if (currentIndex === null) {
            axios.post('http://backendexample.sanbercloud.com/api/student-scores', newData)
                .then(res => {
                    let data = res.data;
                    setdaftarMhs([...daftarMhs, { id: data.id, created_at: data.created_at, updated_at: data.updated_at, name: data.name, course: data.course, score: data.score }]);
                    console.log(daftarMhs);
                })
        } else {              // Update
            axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentIndex}`, newData)
                .then(() => {
                    let data = daftarMhs.find(x=>x.id===currentIndex)
                    data.name = formName;
                    data.course = formCourse;
                    data.score = formScore;
                    setdaftarMhs([...daftarMhs]);
                })
        }
        setformName("");
        setformCourse("");
        setformScore("");
        setcurrentIndex(null)
    }

    const nameChange = (event) => {
        setformName(event.target.value);
    }

    const courseChange = (event) => {
        setformCourse(event.target.value);
    }

    const scoreChange = (event) => {
        setformScore(event.target.value);
    }

    const handleEdit = (event) => {
        let editIndex = parseInt(event.target.value);
        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${editIndex}`)
        .then(res=>{
            let data = res.data;
            setformName(data.name);
            setformCourse(data.course);
            setformScore(data.score);
        })
        setcurrentIndex(editIndex);
    }

    const handleDelete = (event) => {
        let delIndex = parseInt(event.target.value);
        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${delIndex}`)
            .then(() => {
                let newData = daftarMhs.filter(e => { return e.id !== delIndex })
                setdaftarMhs(newData);
            })
    }

    const ShowIndeks = (props) => {
        var nilai = props.score;
        if (nilai > 80) {
            return 'A'
        } else if (nilai < 80 && nilai >= 70) {
            return 'B'
        } else if (nilai < 70 && nilai >= 60) {
            return 'C'
        } else if (nilai < 60 && nilai >= 50) {
            return 'D'
        } else {
            return 'E'
        }
    }

    return (
        <>
            <div className="Daftar-nilai">
                <h1>Daftar Nilai Mahasiswa</h1>
                <table>
                    <thead>
                        <tr>
                            <td>No</td>
                            <td>Nama</td>
                            <td>Mata Kuliah</td>
                            <td>Nilai</td>
                            <td>Indeks Nilai</td>
                            <td>Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            daftarMhs.map((item, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{item.name}</td>
                                        <td>{item.course}</td>
                                        <td>{item.score}</td>
                                        <td><ShowIndeks score={item.score} /></td>
                                        <td><button onClick={handleEdit} value={item.id}>Edit</button>
                                            <button onClick={handleDelete} value={item.id}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
            <div className="Form-nilai-container">
                <h1>Form Harga Daftar Buah</h1>
                <form onSubmit={handleSubmit}>
                    <table>
                        <tbody>
                            <tr>
                                <td className="table-key"><label htmlFor="inputName">Nama :</label></td>
                                <td className="table-val"><input type="text" name="inputName" id="inputName" value={formName} onChange={nameChange} /></td>
                            </tr>
                            <tr>
                                <td className="table-key"><label htmlFor="inputName">Mata Kuliah : </label></td>
                                <td className="table-val"><input type="text" name="inputName" id="inputName" value={formCourse} onChange={courseChange} /></td>
                            </tr>
                            <tr>
                                <td className="table-key"><label htmlFor="inputName">Nilai</label></td>
                                <td className="table-val"><input type="number" name="inputName" id="inputName" placeholder="0" value={formScore} onChange={scoreChange} min="0" max="100" /></td>
                            </tr>
                            <tr>
                                <td colSpan="2" className="table-val"><button>Submit</button></td>
                            </tr>

                        </tbody>
                    </table>
                </form>
            </div>
        </>
    )
}

export default Tugas12;