import React, { useState } from "react";
import './Tugas11.css'

function Tugas11() {
    const [daftarBuah, setdaftarBuah] = useState(
        [
            { nama: "Nanas", hargaTotal: 100000, beratTotal: 4000 },
            { nama: "Manggis", hargaTotal: 350000, beratTotal: 10000 },
            { nama: "Nangka", hargaTotal: 90000, beratTotal: 2000 },
            { nama: "Durian", hargaTotal: 400000, beratTotal: 5000 },
            { nama: "Strawberry", hargaTotal: 120000, beratTotal: 6000 }
        ]
    );

    const [formNama, setformNama] = useState("");
    const [formHarga, setformHarga] = useState("");
    const [formBerat, setformBerat] = useState("");
    const [currentIndex, setcurrentIndex] = useState(-1);

    const handleSubmit = (event)=>{
        event.preventDefault();
        console.log(event.target.value);

        let Data = {nama:"", hargaTotal:0, beratTotal:0};
        Data.nama = formNama;
        Data.hargaTotal = formHarga;
        Data.beratTotal = formBerat;

        let newData = [...daftarBuah];
        // Input Biasa
        if(currentIndex===-1){
            newData = [...newData, ...[Data]];
        }else{              // Update
            newData[currentIndex] = Data;
        }
        setdaftarBuah(newData);
        setformNama("");
        setformHarga("");
        setformBerat("");
        setcurrentIndex(-1)
    }

    const nameChange = (event)=>{
        setformNama(event.target.value);
    }

    const hargaChange = (event)=>{
        setformHarga(parseInt(event.target.value));
    }

    const beratChange = (event)=>{
        setformBerat(parseInt(event.target.value));
    }

    const handleEdit = (event)=>{
        let editIndex = parseInt(event.target.value);
        setcurrentIndex(editIndex);
        setformNama(daftarBuah[editIndex].nama);
        setformHarga(daftarBuah[editIndex].hargaTotal);
        setformBerat(daftarBuah[editIndex].beratTotal);
    }

    const handleDelete = (event)=>{
        let delIndex = parseInt(event.target.value);
        let delItem = daftarBuah[delIndex];
        let newData = daftarBuah.filter(e=>{return e!== delItem})
        setdaftarBuah(newData);
    }

    return (
        <>
            <div className="Daftar-buah">
                <h1>Daftar Harga Buah</h1>
                <table>
                    <thead>
                        <td>No</td>
                        <td>Nama</td>
                        <td>Harga Total</td>
                        <td>Berat Total</td>
                        <td>Harga per kg</td>
                        <td>Aksi</td>
                    </thead>
                    <tbody>
                        {
                            daftarBuah.map((value, index)=>{
                                return(
                                    <tr>
                                        <td>{index+1}</td>
                                        <td>{value.nama}</td>
                                        <td>{value.hargaTotal}</td>
                                        <td>{value.beratTotal/1000} kg</td>
                                        <td>{value.hargaTotal/(value.beratTotal/1000)}</td>
                                        <td><button onClick={handleEdit} value={index}>Edit</button>
                                            <button onClick={handleDelete} value={index}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
            <div className="Form-container">
                <h1>Form Harga Daftar Buah</h1>
                <form onSubmit={handleSubmit}>
                    <table>
                        <tr>
                            <td className="table-key"><label htmlFor="inputName">Nama :</label></td>
                            <td className="table-val"><input type="text" name="inputName" id="inputName" value={formNama} onChange={nameChange} /></td>
                        </tr>
                        <tr>
                            <td className="table-key"><label htmlFor="inputName">harga Total : </label></td>
                            <td className="table-val"><input type="text" name="inputName" id="inputName" value={formHarga} onChange={hargaChange} /></td>
                        </tr>
                        <tr>
                            <td className="table-key"><label htmlFor="inputName">Berat Total (dalam gram)</label></td>
                            <td className="table-val"><input type="text" name="inputName" id="inputName" placeholder="0" value={formBerat} onChange={beratChange} /></td>
                        </tr>
                        <tr>
                            <td colSpan="2" className="table-val"><button>Submit</button></td>
                        </tr>
                    </table>
                </form>
            </div>
        </>
    )
}

export default Tugas11;