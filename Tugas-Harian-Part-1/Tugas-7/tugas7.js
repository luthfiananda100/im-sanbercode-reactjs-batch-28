// SOAL 1
// RELEASE 0

console.log("---------------------SOAL 1------------------");
console.log("---------------------RELEASE 0------------------");

class Animal {
    constructor(name) {
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }
    set legs(x) {
        this._legs = x;
    }

    set cold_blooded(x) {
        this._cold_blooded = x;
    }

    get name() {
        return this._name;
    }

    get legs() {
        return this._legs;
    }

    get cold_blooded() {
        return this._cold_blooded;
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
sheep.legs = 3
console.log(sheep.legs)


console.log("---------------------RELEASE 1------------------");

class Frog extends Animal {
    constructor(name) {
        super(name);
    }

    jump() {
        console.log("Hop Hop");
    }
}

class Ape extends Animal {
    constructor(name) {
        super(name);
    }

    yell() {
        console.log("Auoooooo");
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
sungokong.legs = 2
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)

var kodok = new Frog("buduk")
kodok.jump() // "hop hop"
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)

// SOAL 2
console.log("---------------------SOAL 2------------------");
class Clock {
    // Code di sini
    constructor(x) {
        this._template = x.template;
        this._timer;
        this._date;
        this._hours;
        this._mins;
        this._secs;
    }


    get template() {
        return this._template;
    }

    render() {
        this._date = new Date();
        this._hours = this._date.getHours();
        if (this._hours < 10) this._hours = '0' + this._hours;
        this._mins = this._date.getMinutes();
        if (this._mins < 10) this._mins = '0' + this._mins;
        this._secs = this._date.getSeconds();
        if (this._secs < 10) this._secs = '0' + this._secs;

        var output = this._template.replace('h', this._hours).replace('m', this._mins).replace('s', this._secs);
        console.log(output);
    }

    stop() {
        clearInterval(this._timer);
    }

    start() {
        this.render();
        this._timer = setInterval(() => {
            this.render();
        }, 1000);
    }

}

var clock = new Clock({ template: 'h:m:s' });
clock.start();