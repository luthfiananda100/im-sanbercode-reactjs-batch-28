// SOAL 1
console.log("----------------SOAL 1---------------");

// FUNCTION
function luasPersegiPanjang(p, l){
    return p*l;
}

function kelilingPersegiPanjang(p, l){
    return 2*(p + l);
}

function volumeBalok(p, l, t){
    return p*l*t;
}
 
var panjang= 12
var lebar= 4
var tinggi = 8
 
var luasPersegiPanjang = luasPersegiPanjang(panjang, lebar)
var kelilingPersegiPanjang = kelilingPersegiPanjang(panjang, lebar)
var volumeBalok = volumeBalok(panjang, lebar, tinggi)

console.log(luasPersegiPanjang) 
console.log(kelilingPersegiPanjang)
console.log(volumeBalok)


// SOAL 2
console.log("----------------SOAL 2---------------");

// FUNCTION
function introduce(name, age, address, hobby) {
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!";
}
 
var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan belum jadi, dan saya punya hobby yaitu Gaming!"

// SOAL 3
console.log("----------------SOAL 3--------------");

var arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku" , 1992];

// var keyPeserta = ["nama", "jenis kelamin", "hobi", "tahun lahir"];
var objPeserta = {};
objPeserta.nama = arrayDaftarPeserta[0];
objPeserta.jenisKelamin = arrayDaftarPeserta[1];
objPeserta.hobi = arrayDaftarPeserta[2];
objPeserta.tahunLahir = arrayDaftarPeserta[3];
console.log(objPeserta);

// SOAL 4
console.log("----------------SOAL 4--------------");

var buah = [
    {nama : "Nanas", warna : "Kuning", adaBijinya: "tidak", harga : 9000},
    {nama : "Jeruk", warna : "Oranye", adaBijinya: "ada", harga : 8000},
    {nama : "Semangka", warna : "Hijau & Merah", adaBijinya: "ada", harga : 10000},
    {nama : "Pisang", warna : "Kuning", adaBijinya: "tidak", harga : 5000},
];
var buahFilter = buah.filter(function(item){
    return item.adaBijinya == "tidak";
})
console.log(buahFilter);

// SOAL 5
console.log("----------------SOAL 5--------------");

//FUNCTION 
function tambahDataFilm(nama, durasi, genre, tahun){
    var obj = {}
    obj.nama = nama;
    obj.durasi = durasi;
    obj.genre = genre;
    obj.tahun = tahun;
    dataFilm.push(obj);
}

var dataFilm = [];

tambahDataFilm("LOTR", "2 jam", "action", "1999");
tambahDataFilm("avenger", "2 jam", "action", "2019");
tambahDataFilm("spiderman", "2 jam", "action", "2004");
tambahDataFilm("juon", "2 jam", "horror", "2004");

console.log(dataFilm);

