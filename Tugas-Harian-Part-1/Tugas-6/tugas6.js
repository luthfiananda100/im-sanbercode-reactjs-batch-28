// SOAL 1
console.log("---------------------------SOAL 1-------------------------");
function luasLingkaran(r) {
    const phi = 3.14;
    let luas = phi * r * r;
    return luas;
}
function kelilingLingkaran(r) {
    const phi = 3.14;
    let keliling = 2 * phi * r;
    return keliling;
}

console.log(luasLingkaran(10));
console.log(kelilingLingkaran(10));

// SOAL 2
console.log("---------------------------SOAL 2-------------------------");

const introduce = (...rest) => {
    let panggilan = "Pak";
    if (rest[2] == "Perempuan") {
        panggilan = "Bu";
    }
    return `${panggilan} ${rest[0]} adalah seorang ${rest[3]} yang berusia ${rest[1]} tahun`;
}

//kode di bawah ini jangan dirubah atau dihapus
const perkenalan = introduce("John", "30", "Laki-Laki", "penulis")
console.log(perkenalan) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"

// SOAL 3
console.log("---------------------------SOAL 3-------------------------");
const newFunction = (...rest) => {
    return {
        firstName: rest[0],
        lastName: rest[1],
        fullName: () => {
            console.log(`${rest[0]} ${rest[1]}`);
        }
    };
}

// kode di bawah ini jangan diubah atau dihapus sama sekali
console.log(newFunction("John", "Doe").firstName)
console.log(newFunction("Richard", "Roe").lastName)
newFunction("William", "Imoh").fullName()

// SOAL 4
console.log("---------------------------SOAL 4-------------------------");
let phone = {
    name: "Galaxy Note 20",
    brand: "Samsung",
    year: 2020,
    colors: ["Mystic Bronze", "Mystic White", "Mystic Black"]
}
// kode diatas ini jangan di rubah atau di hapus sama sekali

/* Tulis kode jawabannya di sini */

let {name, brand, year, colors} = phone;
let phoneBrand = brand;
let phoneName = name;
let colorBlack = colors[2];
let colorBronze = colors[0];

// kode di bawah ini jangan dirubah atau dihapus
console.log(phoneBrand, phoneName, year, colorBlack, colorBronze)

// SOAL 5
console.log("---------------------------SOAL 5-------------------------");
let warna = ["biru", "merah", "kuning" , "hijau"];

let dataBukuTambahan= {
  penulis: "john doe",
  tahunTerbit: 2020 
};

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
  warnaSampul:["hitam"]
};
// kode diatas ini jangan di rubah atau di hapus sama sekali

buku.warnaSampul = [...buku.warnaSampul, ...warna];
buku = {...buku, ...dataBukuTambahan};
console.log(buku);
