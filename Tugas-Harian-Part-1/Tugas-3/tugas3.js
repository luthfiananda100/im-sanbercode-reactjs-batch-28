// SOAL 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var kata2 = kataKedua.charAt(0).toUpperCase() + kataKedua.slice(1);
var kata3 = kataKetiga.slice(0,kataKetiga.length-1) + kataKetiga.charAt(kataKetiga.length-1).toUpperCase()
var kata4 = kataKeempat.toUpperCase();

console.log(kataPertama + " " + kata2 + " " + kata3 + " " + kata4);

// SOAL 2
var panjangPersegiPanjang = "8";
var lebarPersegiPanjang = "5";
var alasSegitiga= "6";
var tinggiSegitiga = "7";

var panjang = parseInt(panjangPersegiPanjang);
var lebar = parseInt(lebarPersegiPanjang);
var alas = parseInt(alasSegitiga);
var tinggi = parseInt(tinggiSegitiga);

var kelilingPersegiPanjang = 2*(panjang+lebar);
var luasSegitiga = (alas*tinggi)/2;

console.log(kelilingPersegiPanjang, luasSegitiga);

// SOAL 3
var sentences= 'wah javascript itu keren sekali'; 

var firstWord = sentences.substring(0, 3); 
var secondWord = sentences.substring(4, 14); // do your own! 
var thirdWord = sentences.substring(15, 18); // do your own! 
var fourthWord = sentences.substring(19, 24); // do your own! 
var fifthWord = sentences.substring(25, 33); // do your own! 

console.log('Kata Pertama: ' + firstWord); 
console.log('Kata Kedua: ' + secondWord); 
console.log('Kata Ketiga: ' + thirdWord); 
console.log('Kata Keempat: ' + fourthWord); 
console.log('Kata Kelima: ' + fifthWord);

// SOAL 4
var nilaiJohn = 80;
var nilaiDoe = 50;

// JOHN
if(nilaiJohn>=80){
    console.log("John Indeks = A");
}else if(nilaiJohn>=70 && nilaiJohn<80){
    console.log("John Indeks = B");
}else if(nilaiJohn>=60 && nilaiJohn<70){
    console.log("John Indeks = C");
}else if(nilaiJohn>=50 && nilaiJohn<60){
    console.log("John Indeks = D");
}else{
    console.log("John Indeks = E");
}

//DOE
if(nilaiDoe>=80){
    console.log("Doe Indeks = A");
}else if(nilaiDoe>=70 && nilaiDoe<80){
    console.log("Doe Indeks = B");
}else if(nilaiDoe>=60 && nilaiDoe<70){
    console.log("Doe Indeks = C");
}else if(nilaiDoe>=50 && nilaiDoe<60){
    console.log("Doe Indeks = D");
}else{
    console.log("Doe Indeks = E");
}

// SOAL 5
var tanggal = 10;
var bulan = 2;
var tahun = 1999;

switch(bulan){
    case 1:
        var month = "Januari";
        break;
    case 2:
        var month = "Februari";
        break;
    case 3:
        var month = "Maret";
        break;
    case 4:
        var month = "April";
        break;
    case 5:
        var month = "Mei";
        break;
    case 6:
        var month = "Juni";
        break;
    case 7:
        var month = "Juli";
        break;
    case 8:
        var month = "Agustus";
        break;
    case 9:
        var month = "September";
        break;
    case 10:
        var month = "Oktober";
        break;
    case 11:
        var month = "November";
        break;
    case 12:
        var month = "Desember";
}

console.log(tanggal + " " + month + " " + tahun);