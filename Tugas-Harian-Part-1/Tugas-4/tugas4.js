//SOAL 1
console.log("------------------------------ NOMOR 1 -------------------------------");
let numbers = 2;
console.log("LOOPING PERTAMA");
while (numbers<=20){
    console.log(numbers + " - I love coding");
    numbers += 2
}
console.log("LOOPING KEDUA");
while (numbers>=2){
    console.log(numbers + " - I will become a frontend developer");
    numbers -=2
}

//SOAL 2
console.log("------------------------------ NOMOR 2 -------------------------------");
for(let i=1; i<=20; i++){
    if((i%2==1)&&(i%3==0)){                                 // Ganjil dan Kelipatan 3
        console.log(i + " - I Love Coding");
    } else if(i%2==0){                                      // Genap
        console.log(i + " - Berkualitas");
    } else{                                                 // Ganjil
        console.log(i + " - Santai");
    }
}

// SOAL 3
console.log("------------------------------ NOMOR 3 -------------------------------");
for (let index = 0; index < 7; index++) {
    for (let i = 0; i <= index; i++) {
        process.stdout.write("#")  
    }
    console.log();
}

//SOAL 4
console.log("------------------------------ NOMOR 4 -------------------------------");
var kalimat=["aku", "saya", "sangat", "sangat", "senang", "belajar", "javascript"];
kalimat.splice(2,1);
kalimat.shift();

var sentences = kalimat.join(" ");
console.log(sentences);

//SOAL 5
console.log("------------------------------ NOMOR 5 -------------------------------");
var sayuran = [];
sayuran.push("Kangkung");
sayuran.push("Bayam");
sayuran.push("Buncis");
sayuran.push("Kubis");
sayuran.push("Timun");
sayuran.push("Seledri");
sayuran.push("Tauge");

sayuran.sort();
for (let index = 0; index < sayuran.length; index++) {
    console.log((index+1) + ". " + sayuran[index]);
}
