var filterBooksPromise = require('./promise2.js')

// Lanjutkan code untuk menjalankan function filterBookPromise

const promise1 = (colorful, page)=>{
    filterBooksPromise(colorful, page).then((result)=>{
        console.log(result);
    })
}


const books2 = async (colorful, page) =>{
    try {
        let condition2 = await filterBooksPromise(colorful, page)
        console.log(condition2);
    } catch (e) {
        console.log(e.message);
    }
}

const books3 = async (colorful, page)=>{
    try {
        var output = await filterBooksPromise(colorful, page);
        console.log(output);
    } catch(e) {
        console.log(e.message);
    }

}

promise1(true, 40);
books2(false, 250);
books3(false, 30);

// books1(true, 40);
